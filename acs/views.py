from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def index(request):
	if not request.user.is_authenticated:
		return HttpResponseRedirect(reverse('login'))

	return render(request, 'acs/index.html', {"user": request.user})

def login_view(request):
	if request.method == "POST":
		username = request.POST["username"]
		password = request.POST["password"]
		user = authenticate(request, username=username, password=password)
		if user is not None:
			login(request, user)
			return HttpResponseRedirect(reverse('index'))
		else:
			return render(request, 'acs/login.html', {"message": "Invalid username or password."})

	return render(request, 'acs/login.html', {"message": None})

def logout_view(request):
	logout(request)
	return HttpResponseRedirect(reverse('login'))

def register(request):
	if request.method == "POST":
		first_name = request.POST["first_name"]
		last_name = request.POST["last_name"]
		username = request.POST["username"]
		email = request.POST["email"]
		password = request.POST["password"]
		new_user = User.objects.create_user(first_name=first_name, last_name=last_name, username=username, email=email, password=password)
		new_user.save()

		user = authenticate(request, username=username, password=password)
		login(request, user)
		return HttpResponseRedirect(reverse('index'))

	return render(request, "acs/register.html")