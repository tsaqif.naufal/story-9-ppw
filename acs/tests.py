from django.test import TestCase
from django.urls import resolve
from .views import index, login_view, logout_view, register
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

class Story9UnitTest(TestCase):
	def setUp(self):
		first_name = 'Louise'
		last_name = 'Banks'
		username = 'louise.banks'
		email = 'louisebanks@arrival.com'
		password = 'linguisticsexpert'
		new_user = User.objects.create_user(first_name=first_name, last_name=last_name, username=username, email=email, password=password)
		new_user.save()

	def test_using_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)
	
	def test_url_index_if_authenticated(self):
		self.client.login(username='louise.banks', password='linguisticsexpert')
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_template_if_authenticated(self):
		self.client.login(username='louise.banks', password='linguisticsexpert')
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'acs/index.html')

	def test_context_message_if_authenticated(self):
		self.client.login(username='louise.banks', password='linguisticsexpert')
		response = self.client.get('/')
		user = response.context['user']
		self.assertIsInstance(user, User)
	
	def test_url_index_if_not_authenticated(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 302)

	def test_using_login_template_if_not_authenticated(self):
		response = self.client.get('/', follow=True)
		self.assertTemplateUsed(response, 'acs/login.html')

	def test_using_login_view_function(self):
		found = resolve('/login')
		self.assertEqual(found.func, login_view)

	def test_url_login_if_get_method(self):
		response = self.client.get('/login')
		self.assertEqual(response.status_code, 200)

	def test_using_login_template_if_get_method(self):
		response = self.client.get("/login")
		self.assertTemplateUsed(response, 'acs/login.html')

	def test_context_message_if_get_method(self):
		response = self.client.get('/login')
		message = response.context['message']
		self.assertEqual(message, None)

	def test_url_login_if_post_method_and_auth_failed(self):
		response = self.client.post('/login', data={'username': 'ian.donnelly', 'password': 'theoreticalphysicist'})
		self.assertEqual(response.status_code, 200)

	def test_using_login_template_if_post_method_and_auth_failed(self):
		response = self.client.post('/login', data={'username': 'ian.donnelly', 'password': 'theoreticalphysicist'})
		self.assertTemplateUsed(response, 'acs/login.html')

	def test_context_message_if_post_method_and_auth_failed(self):
		response = self.client.post('/login', data={'username': 'ian.donnelly', 'password': 'theoreticalphysicist'})
		message = response.context['message']
		self.assertEqual(message, 'Invalid username or password.')

	def test_url_login_if_post_method_and_auth_success(self):
		response = self.client.post('/login', data={'username': 'louise.banks', 'password': 'linguisticsexpert'})
		self.assertEqual(response.status_code, 302)

	def test_using_index_template_if_post_method_and_auth_success(self):
		response = self.client.post('/login', data={'username': 'louise.banks', 'password': 'linguisticsexpert'}, follow=True)
		self.assertTemplateUsed(response, 'acs/index.html')

	def test_url_logout_exists(self):
		response = self.client.get('/logout')
		self.assertEqual(response.status_code, 302)

	def test_using_logout_view_function(self):
		found = resolve('/logout')
		self.assertEqual(found.func, logout_view)

	def test_using_login_template(self):
		response = self.client.get('/logout', follow=True)
		self.assertTemplateUsed(response, 'acs/login.html')

	def test_using_register_function(self):
		found = resolve('/register')
		self.assertEqual(found.func, register)

	def test_url_register_if_get_method(self):
		response = self.client.get('/register')
		self.assertEqual(response.status_code, 200)

	def test_url_register_if_post_method(self):
		response = self.client.post('/register', data={'first_name': 'Ian', 'last_name': 'Donnelly', 'username': 'ian.donnelly', 'email': 'iandonnelly@arrival.com', 'password': 'theoreticalphysicist'})
		self.assertEqual(response.status_code, 302)

	def test_using_index_template_if_post_method(self):
		response = self.client.post('/register', data={'first_name': 'Ian', 'last_name': 'Donnelly', 'username': 'ian.donnelly', 'email': 'iandonnelly@arrival.com', 'password': 'theoreticalphysicist'}, follow=True)
		self.assertTemplateUsed(response, 'acs/index.html')


class Story9FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story9FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story9FunctionalTest, self).tearDown()

	def test_input_status(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/register')

		first_name = selenium.find_element_by_name('first_name')
		last_name = selenium.find_element_by_name('last_name')
		username = selenium.find_element_by_name('username')
		email = selenium.find_element_by_name('email')
		password = selenium.find_element_by_name('password')
		register_button = selenium.find_element_by_class_name('btn')
		time.sleep(3)

		first_name.send_keys('Louise')
		time.sleep(3)
		last_name.send_keys('Banks')
		time.sleep(3)
		username.send_keys('louise.banks')
		time.sleep(3)
		email.send_keys('louisebanks@arrival.com')
		time.sleep(3)
		password.send_keys('linguisticsexpert')
		time.sleep(3)
		register_button.click()
		time.sleep(5)

		logout_button = selenium.find_element_by_class_name('btn')
		logout_button.click()
		time.sleep(3)

		username = selenium.find_element_by_name('username')
		password = selenium.find_element_by_name('password')
		login_button = selenium.find_element_by_class_name('btn')
		time.sleep(3)

		username.send_keys('ian.donnelly')
		time.sleep(3)
		password.send_keys('theoreticalphysicist')
		time.sleep(3)
		login_button.click()
		time.sleep(3)

		username = selenium.find_element_by_name('username')
		password = selenium.find_element_by_name('password')
		login_button = selenium.find_element_by_class_name('btn')
		time.sleep(3)

		username.send_keys('louise.banks')
		time.sleep(3)
		password.send_keys('linguisticsexpert')
		time.sleep(3)
		login_button.click()
		time.sleep(5)

		logout_button = selenium.find_element_by_class_name('btn')
		logout_button.click()
		time.sleep(5)